/// Prompt:
/// The objective is to take a positive integer of at least 3 digits and return the next largest
/// integer formed from same digits. If no larger integer can be formed, return false
///
/// Note: I use an enum to return `None` instead of false, since Rust has a good way to express an
/// error or non-result, and because Rust is a little stricter with types
///
/// Approach:
/// 1) In order to create a digit permutation with a larger value, a digit with less significance
///    should be swapped with a digit of greater significance but less value.
/// 2) In order to make the next highest value, the check for slots to be swapped should be
///    performed from the least significant digit.
///
/// (My first pass I did with swapping neighbors before I realized it wasn't right. Ended up doing
/// internet research for complete solution)
///
/// Algorithm:
/// 1) from least sig digit, find first digit which is less than previous digit.
/// 2) swap this digit with minimum less-significant digit greater than the digit
/// 3) sort less significant digits

use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let input = std::env::args()
        .nth(1)
        .ok_or_else(|| "Please provide an integer input")?
        .parse::<u64>()
        .map_err(|_| "Input not a positive integer")?;

    // doing the check for 3 digit integer outside of fn
    if input < 100 {
        return Err("Input must be 3 digits or more".into());
    }

    let res = next_permutation_naive(input);

    println!("{:?}", res);

    Ok(())
}

fn next_permutation_naive(n: u64) -> Option<u64> {
    // slide window from least significant
    // to most significant digits to find first
    // possibility of a swap
    //
    // unfortunately allocating again, but this
    // is naive approach
    let mut digits: Vec<_> = n.to_string()
        .chars()
        .map(|d| d.to_digit(10).expect("logic already checked for integer"))
        .collect();
    let mut windows = digits.windows(2).enumerate().rev();

    // swap this idx with the lower place, if swappable
    // neighbors found
    let mut swap_idx_digit = None;

    while let Some((idx, digit_neighbors)) = windows.next() {
        // less significant is on "right" side of array
        let d0 = digit_neighbors[1];
        let d1 = digit_neighbors[0];

        if d0 > d1 {
            swap_idx_digit = Some((idx, d1));
            break;
        }
    }

    // performs the swap and sort if swap_index is_some
    // Otherwise returns None
    swap_idx_digit
        .map(|(idx, digit)| {
            // split, less sig digits to right
            let (left, right) = digits.split_at_mut(idx + 1);

            // of less sig digits, get index of min for swap.
            // Don't mind the *, they're just dereferencing
            // references to the actual value.
            let (swap2_idx, _) = right.iter().enumerate()
                .filter(|(_idx, d)| **d > digit)
                .min_by_key(|(_idx, d)| *d)
                .expect("logic error, must always be able to split if swap_idx is_some");

            // swap two locations
            std::mem::swap(&mut left[idx], &mut right[swap2_idx]);

            // sort less sig side
            right.sort();

            // now convert array representation to an integer
            left.iter().chain(right.iter())
                .rev()
                .enumerate()
                .map(|(i, d)| *d as u64 * 10u64.pow(i as u32))
                .sum()

        })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_cases() {
        assert_eq!(next_permutation_naive(112), Some(121));
        assert_eq!(next_permutation_naive(110), None);
        assert_eq!(next_permutation_naive(34512), Some(34521));

        assert_eq!(next_permutation_naive(14651), Some(15146));
        assert_eq!(next_permutation_naive(534976), Some(536479));
    }
}
