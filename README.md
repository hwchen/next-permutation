## Next highest integer from digit permutation

My most comfortable language is Rust, so I used it for this problem. Feel free to ask questions, but I tried to comment the code pretty well.

### Usage
Ordinarily, I'd package the program into a binary for download (through direct download or ppa, etc.). Since you don't know me and might not want to run a random binary I provide you, here's two other options:

### build from source
download rust version manager
```
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
then clone this repo, go to repo root, and:
```
$ cargo run <your integer input here>
```
or, to run tests
```
$ cargo test
```

### run in online rust playground
copy and past the code to https://play.rust-lang.org/ and click `test`. The `main` fn used for the binary will be unused, but you can still run the tests.

permalink to playground code: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=de10ee3e48fcebef89c607af6ae1d4ac
